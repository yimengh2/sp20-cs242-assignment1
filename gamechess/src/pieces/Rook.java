package pieces;
import java.util.ArrayList;


public class Rook extends Piece{
	public Rook(Position piecePosition, PieceColor pColor, boolean isFirstMove) {
		super(piecePosition, pColor, isFirstMove);
	}
	
	
	/*
	 * move horizontally or vertically
	* Setup valid move position vector for Rook Piece.
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	@Override
	public void setValidMovesList(int rank, int file, Piece[][]chessboard) {
		
		validMovesList = new ArrayList<Position>();
		PieceColor pColor = chessboard[rank][file].getPieceColor();
		
		for(int i = rank + 1; i < 8; i++) {//move to right
			if(move(i, chessboard, pColor, file) == true) {
				break;
			}
		}
		
		for(int i = rank - 1; i >= 0; i--) {//move to left
			if( move(i, chessboard, pColor, file) == true) {
				break;
			}
		}
		
		for(int j = file + 1; j < 8; j++) {//move up
			if( move(rank, chessboard, pColor, j) == true) {
				break;
			}
		}
		
		for(int j = file - 1 ; j >= 0; j--) {//move down
			if( move(rank, chessboard, pColor, j) == true) {
				break;
			}
		}
	}
	
	/*
	 *helper function
	 *@param file
	 *@param chessboard
	 *@param pColor
	 *@param j
	 *@return boolean  
	*/
	private boolean move(int rank, Piece[][] chessboard, PieceColor pColor, int j) {
		if (!outOfBound(rank, j)) {
			if(chessboard[rank][j] != null) {//not empty
				if(chessboard[rank][j].getPieceColor() == pColor) {//same color
					return true;
				}
				else {	//different color
					validMovesList.add(new Position (rank, j));
					return true;
				}
			}
			else {	//empty
				validMovesList.add(new Position(rank, j));
				return false;
			}
		}
		else {
			return true;
		}
	}
}