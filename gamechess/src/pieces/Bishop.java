package pieces;
import java.util.ArrayList;


public class Bishop extends Piece{
	public Bishop(Position piecePosition, PieceColor pColor, boolean isFirstMove) {
		super(piecePosition, pColor, isFirstMove);
	}
	
	/*
	 * Bishop piece can move diagnally
	* Setup valid move position vector for Bishop Piece.
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	@Override
	public void setValidMovesList(int rank, int file, Piece[][]chessboard) {
		validMovesList = new ArrayList<Position>();
		PieceColor pColor = chessboard[rank][file].getPieceColor();
	
		int new_file = file;
		for(int i = rank + 1; i < 8; i++) {		//move to up right
			new_file = new_file + 1;
			if(moveBishop(chessboard, pColor, new_file, i)) {
				break;
			}
		}
		
		int new_file2 = file;
		for(int i = rank + 1; i < 8; i++) {		//move to down right
			new_file2 = new_file2 - 1;
			if(moveBishop(chessboard, pColor, new_file2, i)) {
				break;
			}
		}
		
		int new_file3 = file;
		for(int i = rank - 1; i >= 0; i--) {	//move to up left
			new_file3 = new_file3 + 1;
			if(moveBishop(chessboard, pColor, new_file3, i)) {
				break;
			}
		}
		
		int new_file4 = file;
		for(int i = rank - 1; i >= 0; i--) {	//move to down left
			new_file4 = new_file4 - 1;
			if(moveBishop(chessboard, pColor, new_file4, i)) {
				break;
			}
		}
	}

	/*
	* helper function for common movement
	* @param chessboard
	* @param pColor
	* @param new_file
	* @param i
	* @return boolean
	*/
	private boolean moveBishop(Piece[][] chessboard, PieceColor pColor, int new_file, int i) {
		boolean shouldBreak = false;
		if (!outOfBound(i, new_file)) {
			if(chessboard[i][new_file] != null) {	//not empty
				if(chessboard[i][new_file].getPieceColor() == pColor) {	//same color
					shouldBreak = true;
					return shouldBreak;
				}
				else {	//different color
					validMovesList.add(new Position (i, new_file));
					shouldBreak = true;
					return shouldBreak;
				}
			}
			else {	//empty
				validMovesList.add(new Position(i, new_file));
				return shouldBreak;
			}
		}
		else {	//outofbound
			shouldBreak = true;
			return shouldBreak;
		}
	}
}