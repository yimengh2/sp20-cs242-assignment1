package pieces;

import java.util.ArrayList;


public class Rider extends Piece{
	
	public Rider(Position piecePosition, PieceColor pColor, boolean isFirstMove) {
		super(piecePosition, pColor, isFirstMove);
	}
	
	/*
	* Setup valid move position vector for my Rider Piece.
	* Can move any step in one direction and can leap diagonally when a intermedian piece exists, as long as the dest piece is opponent or null
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	@Override
	public void setValidMovesList(int rank, int file, Piece[][]chessboard) {
		validMovesList = new ArrayList<Position>();
		
		PieceColor pColor = chessboard[rank][file].getPieceColor();
		
		int new_file = file;
		for(int i = rank + 2; i < 8; i+=2) {		//move to up right
			new_file = new_file + 2;
			if (!outOfBound(i, new_file)) {
				if(chessboard[i - 1][new_file - 1]!=null) {
					commonMove(chessboard, pColor, new_file, i);
				}
			}
		}
		
		int new_file2 = file;
		for(int i = rank + 2; i < 8; i+=2) {		//move to down right
			new_file2 = new_file2 - 2;
			if (!outOfBound(i, new_file2)) {
				if(chessboard[i - 1][new_file2 + 1]!=null) {
					commonMove(chessboard, pColor, new_file2, i);
				}
			}	
			
		}
		
		int new_file3 = file;
		for(int i = rank - 2; i >= 0; i-=2) {	//move to up left
			new_file3 = new_file3 + 2;
			if (!outOfBound(i, new_file3)) {
				if(chessboard[i + 1][new_file3 - 1]!=null) {
					commonMove(chessboard, pColor, new_file3, i);
				}
			}
		}
		
		int new_file4 = file;
		for(int i = rank - 2; i >= 0; i-=2) {	//move to down left
			new_file4 = new_file4 - 2;
			if (!outOfBound(i, new_file4)) {
				if(chessboard[i + 1][new_file4 + 1]!=null) {
					commonMove(chessboard, pColor, new_file4, i);
				}
			}
		}
	}

	/*
	 *helper function
	 *@param file
	 *@param chessboard
	 *@param pColor
	 *@param i
	 *@return boolean  
	*/
	private void commonMove(Piece[][] chessboard, PieceColor pColor, int new_file, int i) {
		if(chessboard[i][new_file]==null) {
			validMovesList.add(new Position(i, new_file));
		}
		else {
			if(chessboard[i][new_file].getPieceColor() != pColor) {
				validMovesList.add(new Position(i, new_file));
			}
		}
	}

}