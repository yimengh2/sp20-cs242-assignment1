package pieces;
import java.util.ArrayList;

public class Pawn extends Piece{
	public Pawn(Position piecePosition, PieceColor pColor, boolean isFirstMove) {
		super(piecePosition, pColor, isFirstMove);
	}

	/*
	 * Move forward and capture diagonally forward
	 * 	
	* Setup valid move position vector for Pawn Piece.
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	@Override
	public void setValidMovesList(int rank, int file, Piece[][]chessboard) {
		//System.out.println("pawn1");
		validMovesList = new ArrayList<Position>();
		if(chessboard[rank][file].getPieceColor() == PieceColor.Black) {
			if(chessboard[rank][file].firstMove()) {
				if(!outOfBound(rank + 2, file) && chessboard[rank + 1][file] == null) {
					validMovesList.add(new Position (rank + 2, file));
				}
				commonMove(rank, file, chessboard);
			}
			else {
				commonMove(rank, file, chessboard);
			}	
		}
		else {
			//System.out.println("pawn");
			if(chessboard[rank][file].firstMove()) {
				if(!outOfBound(rank - 2, file) && chessboard[rank - 1][file] == null) {
					validMovesList.add(new Position (rank - 2, file));
				}
				commonMoveWhite(rank, file, chessboard);
			}
			else {
				commonMoveWhite(rank, file, chessboard);
			}	
		}
	}
	
	/*
	 * Movement of Pawns regardless of isFirstMove
	 * @param rank
	 * @param file
	 * @param chessboard
	 * @return void
	 */
	private void commonMoveWhite(int rank, int file, Piece[][] chessboard) {
		if(!outOfBound(rank - 1, file)) {
			validMovesList.add(new Position (rank - 1, file));
		}
		if(!outOfBound(rank - 1, file + 1) && chessboard[rank - 1][file + 1] != null && chessboard[rank - 1][file + 1].getPieceColor() != chessboard[rank][file].getPieceColor()) {
			validMovesList.add(new Position (rank - 1, file+1));
		}
		if(!outOfBound(rank - 1, file - 1) && chessboard[rank - 1][file - 1] != null && chessboard[rank - 1][file - 1].getPieceColor() != chessboard[rank][file].getPieceColor()) {
			validMovesList.add(new Position (rank - 1, file - 1));
		}
	}
	
	/*
	 * Movement of Pawns regardless of isFirstMove
	 * @param rank
	 * @param file
	 * @param chessboard
	 * @return void
	 */
	private void commonMove(int rank, int file, Piece[][] chessboard) {
		if(!outOfBound(rank + 1, file)) {
			validMovesList.add(new Position (rank + 1, file));
		}
		if(!outOfBound(rank + 1, file + 1) && chessboard[rank + 1][file + 1] != null && chessboard[rank + 1][file + 1].getPieceColor() != chessboard[rank][file].getPieceColor()) {
			validMovesList.add(new Position (rank+1, file+1));
		}
		if(!outOfBound(rank + 1, file - 1) && chessboard[rank + 1][file - 1] != null && chessboard[rank + 1][file - 1].getPieceColor() != chessboard[rank][file].getPieceColor()) {
			validMovesList.add(new Position (rank + 1, file - 1));
		}
	}
}