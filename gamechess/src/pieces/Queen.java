package pieces;
import java.util.ArrayList;


public class Queen extends Piece{
	public Queen(Position piecePosition, PieceColor pColor, boolean isFirstMove) {
		super(piecePosition, pColor, isFirstMove);
	}
	@Override
	
	/*
	* Setup valid move position vector for Bishop Piece.
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	public void setValidMovesList(int rank, int file, Piece[][]chessboard) {
		
		validMovesList = new ArrayList<Position>();
		PieceColor pColor = chessboard[rank][file].getPieceColor();
		
		for(int i = rank + 1; i < 8; i++) {			//move to right
			if(commonMove(file, chessboard, pColor, i) == true) {
				break;
			}
		}
		
		for(int i = rank - 1; i >= 0; i--) {		//move to left
			if(commonMove(file, chessboard, pColor, i) == true) {
				break;
			}
		}
		
		for(int j = file + 1; j < 8; j++) {		//move up
			if(commonMove(j, chessboard, pColor, rank) == true) {
				break;
			}
		}
		
		for(int j = file - 1 ; j >= 0; j--) {		//move down
			if(commonMove(j, chessboard, pColor, rank) == true) {
				break;
			}
		}
		
		int new_file = file;
		for(int i = rank + 1; i < 8; i++) {		//move to up right
			new_file = new_file + 1;
			if( commonMove(new_file, chessboard, pColor, i) == true) {
				break;
			}
		}
		
		int new_file2 = file;
		for(int i = rank + 1; i < 8; i++) {		//move to down right
			new_file2 = new_file2 - 1;
			if( commonMove(new_file2, chessboard, pColor, i) == true) {
				break;
			}
		}
		
		int new_file3 = file;
		for(int i = rank - 1; i >= 0; i--) {	//move to up left
			new_file = new_file3 + 1;
			if( commonMove(new_file3, chessboard, pColor, i) == true) {
				break;
			}
		}
		
		int new_file4 = file;
		for(int i = rank - 1; i >= 0; i--) {	//move to down left
			new_file = new_file4 - 1;
			commonMove(new_file4, chessboard, pColor, i);
		}
	}
	
	/*
	 *helper function
	 *@param file
	 *@param chessboard
	 *@param pColor
	 *@param i
	 *@return boolean  
	*/
	private boolean commonMove(int file, Piece[][] chessboard, PieceColor pColor, int i) {
		if (!outOfBound(i, file)) {
			if(chessboard[i][file] != null) {	//not empty
				if(chessboard[i][file].getPieceColor() == pColor) {	//same color
					return true;
				}
				else {	//different color
					validMovesList.add(new Position (i, file));
					return true;
				}
			}
			else {	//empty
				validMovesList.add(new Position(i, file));
				return false;
			}
		}
		else {	//outofbound
			return true;
		}
	}	
}