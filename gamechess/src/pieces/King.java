package pieces;

import java.util.ArrayList;


public class King extends Piece{
	
	public King(Position piecePosition, PieceColor pColor, boolean isFirstMove) {
		super(piecePosition, pColor, isFirstMove);
	}
	
	/*
	* Setup valid move position vector for King Piece.
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	@Override
	public void setValidMovesList(int rank, int file, Piece[][]chessboard) {
		validMovesList = new ArrayList<Position>();
		
		//move any position 1 step
		int position_x[] = {1, -1, 0, 0, 1, 1, -1, -1};
		int position_y[] = {0, 0, 1, -1, 1, -1, 1, -1};
		for (int i = 0;i < 8; i++) {
			int dest_rank = rank + position_x[i];
			int dest_file = file + position_y[i];

			if (!outOfBound(dest_rank, dest_file)) {
				if (chessboard[dest_rank][dest_file] == null) { 
					validMovesList.add(new Position(dest_rank, dest_file));
				}
				else {
					if (chessboard[dest_rank][dest_file].getPieceColor()!=chessboard[rank][file].getPieceColor()) {
						validMovesList.add(new Position(dest_rank, dest_file));
					}
				}
			}
		}
	}
}