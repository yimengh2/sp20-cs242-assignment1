package pieces;
import java.util.ArrayList;


public class Knight extends Piece{
	public Knight(Position piecePosition, PieceColor pColor, boolean isFirstMove) {
		super(piecePosition, pColor, isFirstMove);
	}
	@Override
	
	/*
	* Setup valid move position vector for Knight Piece.
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	public void setValidMovesList(int rank, int file, Piece[][]chessboard) {
		validMovesList = new ArrayList<Position>();
		
		//move any L position
		int position_rank[] = {2, 1, -1, -2, -2, -1, 1, 2};
		int position_file[] = {1, 2, 2, 1, -1, -2, -2, -1};
		for (int i = 0;i < 8; i++) {
			int dest_rank = rank + position_rank[i];
			int dest_file = file + position_file[i];
			if (!outOfBound(dest_rank, dest_file)) {
				validMovesList.add(new Position(dest_rank, dest_file));
			}
		}
	}
}