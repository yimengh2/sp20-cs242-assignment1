package pieces;

/*
 *enum class to identify piece color 
*/
public enum PieceColor{
	Black,
	White
}