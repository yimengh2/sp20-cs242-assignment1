package pieces;

public class Position{
	private int rank;
	private int file;
	
	/*
	 *Constructor for position with input rank and file
	*/
	public Position(int rank, int file){
		this.rank = rank;
		this.file = file;
	}
	
	/*
	 *getter for rank 
	 *@return int
	*/
	public int getRank() {
		return rank;
	}
	
	/*
	 *getter for file
	 *@return int
	*/
	public int getFile() {
		return file;
	}
}