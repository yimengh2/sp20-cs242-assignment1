package pieces;

import java.util.ArrayList;


public class Fertz extends Piece{
	
	public Fertz(Position piecePosition, PieceColor pColor, boolean isFirstMove) {
		super(piecePosition, pColor, isFirstMove);
	}
	
	/*
	* Setup valid move position vector for my Fertz Piece.
	* Only move when the dest has opponent piece
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	@Override
	public void setValidMovesList(int rank, int file, Piece[][]chessboard) {
		validMovesList = new ArrayList<Position>();
		
		
		int[] position_x = {-1, -1, 1, 1};
		int[] position_y = {-1, 1, -1, 1};
		
		for (int i = 0;i < 4; i++) {
			int dest_rank = rank + position_x[i];
			int dest_file = file + position_y[i];

			if (!outOfBound(dest_rank, dest_file)) {
				//if dest position is null, there must has one same color piece in [dest_rank-1][dest_file] for valid move
				if (chessboard[dest_rank][dest_file] == null) { 
					if(chessboard[dest_rank-1][dest_file]!= null && chessboard[dest_rank-1][dest_file].getPieceColor()==chessboard[rank][file].getPieceColor()) {
						validMovesList.add(new Position(dest_rank, dest_file));
					}
				}
				else {
					if (chessboard[dest_rank][dest_file] != null && chessboard[dest_rank][dest_file].getPieceColor()!=chessboard[rank][file].getPieceColor()) {
						validMovesList.add(new Position(dest_rank, dest_file));
					}
					
				}
			}
		}	
	}
}