package pieces;
import java.util.ArrayList;

public abstract class Piece{
	protected Position pos;
	protected PieceColor pColor;
	public ArrayList<Position> validMovesList;
	protected String pieceName;
	protected boolean isFirstMove; 	/* check for pawn */
	
	
	/*
	* Construct a <em>Piece</em> suitable for holding Postion, pColor and isFirstMove.
	*/
	public Piece(Position pos, PieceColor pColor, boolean isFirstMove) {
		this.pos = pos;
		this.pColor = pColor;
		this.isFirstMove = isFirstMove;
	}
	
	
	public void setPieceName(String s) {
		pieceName = s;
	}
	
	public String getPieceName() {
		return pieceName;
	}
	/*
	 * setter for isFirstMove
	* @param b
	* 	boolean of isFirstmove
	* @return void
	*/
	public void setFirstMove(boolean b) {
		isFirstMove = b;
	}
	
	/*
	 * setter for position
	* @param pos
	* @return void
	*/
	public void setPosition(Position pos) {
		this.pos = pos;
	}
	
	/*
	 * setter for piececolor
	* @param c
	* @return void
	*/
	public void setPieceColor(PieceColor c) {
		pColor = c;
	}
	
	/*
	 * getter for piececolor
	* @param
	* @return PieceColor
	*/
	public PieceColor getPieceColor() {
		return pColor;
	}
	
	/*
	* getter for isFirstMove
	* @param
	* @return boolean
	*/
	public boolean firstMove() {
		return isFirstMove;
	}
	
	/*
	* getter for pieceRank
	* @param
	* @return int
	*/
	public int getPieceRank() {
		return pos.getRank();
	}
	
	/*
	 * getter for pieceFile
	* @param
	* @return int
	*/
	public int getPieceFile() {
		return pos.getFile();
	}
	
	/*
	* Setup valid move position vector for Bishop Piece.
	* @param rank
	* @param file
	* @param chessboard
	* @return void
	*/
	public abstract void setValidMovesList(int rank, int file, Piece[][]chessboard);

	/*
	 * Check if the destination is in validmoveslist.
	* @param rank
	* @param file
	* @return boolean
	*/
	public boolean isLegalMove(int rank, int file) {
		if(validMovesList!=null) {
			for (Position p:validMovesList) {
				if(p.getRank() == rank && p.getFile() == file) {
					return true;
				}
			}
		}
		return false;
	}
	
	/*
	 * check outofbound of position
	* @param rank
	* @param file
	* @return boolean
	*/
	public boolean outOfBound (int rank, int file) {
		if (rank < 0 || file < 0 || rank >=8 || file >= 8) {
			return true;
		}
		else return false;
	}
}