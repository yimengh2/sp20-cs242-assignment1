//package gui;
//import java.awt.Color;
//import java.awt.Dimension;
//import java.awt.GridLayout;
//import java.awt.Image;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.image.BufferedImage;
//import java.net.URL;
//
//import javax.imageio.ImageIO;
//import javax.swing.JButton;
//import javax.swing.JFrame;
//import javax.swing.JLabel;
//import javax.swing.JMenu;
//import javax.swing.JMenuBar;
//import javax.swing.JMenuItem;
//import javax.swing.JPanel;
//
//import board.Board;
//
//import javax.swing.BorderFactory;
//import javax.swing.ImageIcon;
//import java.lang.*;
////public class BoardGui extends JFrame{
//	
//	private Board chessBoard = new Board();
//	private JButton[][] grids = new JButton[8][8];
//	public BoardPanel bPanel;
//	
//	private class BoardPanel extends JPanel{
//
//	    private static final long serialVersionUID = 1L;
//	    
//		BoardPanel(){
//			setLayout(new GridLayout(8,8));
//			setBorder(BorderFactory.createEmptyBorder(-4,-2,-4,-2));
//			
//			createEmptyBoard(grids);
//			for( int rank = 0; rank < 8; rank++) {
//				for (int file = 0; file < 8; file ++) {
//					////????
//					add(grids[7-rank][file]);
//				}
//			}
//		}
//	}
//	
//	public void createEmptyBoard(JButton[][] grids) {
//		for(int rank = 0; rank < 8; rank++) {
//			for (int file = 0; file < 8; file++) {
//				JButton button = new JButton();
//				if((rank + file) % 2==0){
//					button.setBackground(Color.PINK);
//					button.setOpaque(true);
//				}
//				else {
//					button.setBackground(Color.white);
//					button.setOpaque(true);
//				}
//				grids[rank][file] = button;
//				//grids[rank][file].addActionListener(this);
//			}
//		}
//	}
//	
//	private JMenuBar createMenu() {
//		JMenuBar menu = new JMenuBar();
//		////change structure
//		menu.add(menuHelper());
//		return menu;
//	}
//	
//	
//	private JMenu menuHelper() {
//		JMenu menu = new JMenu("Menu");
//		
//		JMenuItem startGame = new JMenuItem("Start Game");
//		
//		startGame.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent event) {
//				setVisible(false);
//				//Gui game = new Gui(false);
//			}
//		});
//		
//		menu.add(startGame);
//		
//		
//		JMenuItem restartGame = new JMenuItem("ReStart Game");
//		restartGame.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent event) {
//				setVisible(false);
//				//Gui game = new Gui(true);
//			}
//		});
//		
//		menu.add(restartGame);
//		return menu;
//	}
//	
//	
//    public static void main(String[] args) {
//    	
//		this.bPanel = new BoardPanel();
//		final JMenuBar menuBar = createMenu();
//		this.setJMenuBar(menuBar);
//		
//		this.setSize(new Dimension(600,600));
//		
//    	ImageIcon blackRook = new ImageIcon("src/gui/chess_rdt60.png");
//    	ImageIcon blackBishop = new ImageIcon("src/gui/chess_bdt60.png");
//    	ImageIcon blackQueen = new ImageIcon("src/gui/chess_qdt60.png");
//    	ImageIcon blackKnight = new ImageIcon("src/gui/chess_ndt60.png");
//    	ImageIcon blackKing = new ImageIcon("src/gui/chess_kdt60.png");
//    	ImageIcon blackPawn = new ImageIcon("src/gui/chess_pdt60.png");
//    	
//    	ImageIcon whiteRook = new ImageIcon("src/gui/chess_rlt60.png");
//    	ImageIcon whiteBishop = new ImageIcon("src/gui/chess_blt60.png");
//    	ImageIcon whiteQueen = new ImageIcon("src/gui/chess_qlt60.png");
//    	ImageIcon whiteKnight = new ImageIcon("src/gui/chess_nlt60.png");
//    	ImageIcon whiteKing = new ImageIcon("src/gui/chess_klt60.png");
//    	ImageIcon whitePawn = new ImageIcon("src/gui/chess_plt60.png");
//    	
//    	
//    	
//    	
//    	
//    	
//    	
//    	
//    	 /////to do clickable
//    	 
//        JFrame jFrame=new JFrame("board");
//        //set size of board
//        jFrame.setSize(450,450);
//       
//        JButton[][] jButton = new JButton[9][9];
//        
//        int m=9;
//        for(int i=0;i<m;i++){
//            for(int j=0;j<m;j++){
//            	
//                JLabel jLabel=new JLabel();
//
//                //jButton[i][j].setSize(50,50);
//                //jButton[i][j].setLocation(i*50,j*50);
//                JButton button = new JButton();
//                button.setSize(50,50);
//                button.setLocation(i*50,j*50);
//                jLabel.setSize(50,50);
//
//                jLabel.setLocation(i*50,j*50);
//
//                if((i+j)%2==0){
//                	if (i<=7 && j<=7) {
//	                    jLabel.setBackground(Color.PINK);
//	            
//	                    if(i == 0 && j == 0) {
//	                    	
//	                    	button.setIcon(blackRook);
//	                    }
//	                    if(i == 2 && j == 0) {
//	                    	button.setIcon(blackBishop);
//	                    }
//	                    if(i == 4 && j == 0) {
//	                    	button.setIcon(blackKing);
//	                    }
//	                    if(i == 6 && j == 0) {
//	                    	button.setIcon(blackKnight);
//	                    }
//	                    
//	                    if(i == 1 && j == 7) {
//	                    	button.setIcon(whiteKnight);
//	                    }
//	                    if(i == 3 && j == 7) {
//	                    	button.setIcon(whiteQueen);
//	                    }
//	                    if(i == 5 && j == 7) {
//	                    	button.setIcon(whiteBishop);
//	                    }
//	                    if(i == 7 && j == 7) {
//	                    	button.setIcon(whiteRook);
//	                    }
//	                    
//	                    
//	                    if(j == 1) {
//	                    	button.setIcon(blackPawn);
//	                    }
//	                    if(j == 6) {
//	                    	button.setIcon(whitePawn);
//	                    }
//                	}
//                    jLabel.setOpaque(true);
//                }else{
//                	if (i<=7 && j<=7) {
//                		jLabel.setBackground(Color.lightGray);
//                		button.setBackground(Color.lightGray);
//	                	
//	                    //jLabel.setIcon();
//	                    if(i == 1 && j == 0) {
//	                    	button.setIcon(blackKnight);
//	                    }
//	                    if(i == 3 && j == 0) {
//	                    	button.setIcon(blackQueen);
//	                    }
//	                    if(i == 5 && j == 0) {
//	                    	button.setIcon(blackBishop);
//	                    }
//	                    if(i == 7 && j == 0) {
//	                    	button.setIcon(blackRook);
//	                    }
//	                    
//	                    if(i == 0 && j == 7) {
//	                    	button.setIcon(whiteRook);
//	                    }
//	                    if(i == 2 && j == 7) {
//	                    	button.setIcon(whiteBishop);
//	                    }
//	                    if(i == 4 && j == 7) {
//	                    	button.setIcon(whiteKing);
//	                    }
//	                    if(i == 6 && j == 7) {
//	                    	button.setIcon(whiteKnight);
//	                    }
//	                    
//	                    if(j == 1) {
//	                    	button.setIcon(blackPawn);
//	                    }
//	                    if(j == 6) {
//	                    	button.setIcon(whitePawn);
//	                    }
//                	}
//                    jLabel.setOpaque(true);
//                    
//                }
//                button.setOpaque(true);
//                jButton[i][j] = button;
//                jFrame.add(jLabel);
//                jFrame.add(jButton[i][j]);
//            }
//        }
//        jFrame.setVisible(true);
//
//        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//    }
//
//}