package gui;

import board.Board;
import board.Player;

import pieces.PieceColor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Gui extends JFrame implements ActionListener{
	
	ImageIcon blackRook = new ImageIcon("src/gui/chess_rdt60.png");
	ImageIcon blackBishop = new ImageIcon("src/gui/chess_bdt60.png");
	ImageIcon blackQueen = new ImageIcon("src/gui/chess_qdt60.png");
	ImageIcon blackKnight = new ImageIcon("src/gui/chess_ndt60.png");
	ImageIcon blackKing = new ImageIcon("src/gui/chess_kdt60.png");
	ImageIcon blackPawn = new ImageIcon("src/gui/chess_pdt60.png");
	
	ImageIcon whiteRook = new ImageIcon("src/gui/chess_rlt60.png");
	ImageIcon whiteBishop = new ImageIcon("src/gui/chess_blt60.png");
	ImageIcon whiteQueen = new ImageIcon("src/gui/chess_qlt60.png");
	ImageIcon whiteKnight = new ImageIcon("src/gui/chess_nlt60.png");
	ImageIcon whiteKing = new ImageIcon("src/gui/chess_klt60.png");
	ImageIcon whitePawn = new ImageIcon("src/gui/chess_plt60.png");
	
	private Board chessboard = new Board();
	private JButton[][] grids = new JButton[8][8];
	private BoardPanel bPanel;
	private Player playerWhite;
	private Player playerBlack;
	private PieceColor turn = PieceColor.White;
	
	//initialize boardPanel
	private class BoardPanel extends JPanel{
		BoardPanel(){
			setLayout(new GridLayout(8,8));
			createEmptyBoard(grids);
			
			for( int rank = 0; rank < 8; rank++) {
				for (int file = 0; file < 8; file ++) {
					add(grids[rank][file]);
				}
			}
		}
	}
	
	
	/*
	 * create menu
	* @param void
	* @return JMenuBar
	*/
	private JMenuBar createMenu() {
		JMenuBar menu = new JMenuBar();
		////change structure
		menu.add(menuHelper());
		return menu;
	}
	
	/*
	 * Create JMenuItem under Menu
	* @param void
	* @return JMenu
	*/
	private JMenu menuHelper() {
		JMenu menu = new JMenu("Menu");
		
		//create JmenuItem "Start Game" Under menu, start game
		JMenuItem startGame = new JMenuItem("Start Game");
		//System.out.println("79");
		startGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				//System.out.println("83");
				Gui game = new Gui(false);
			}
		});
		
		menu.add(startGame);
		
		//create JmenuItem "ReStart Game" Under menu, restart game
		JMenuItem restartGame = new JMenuItem("ReStart Game");
		restartGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				Gui game = new Gui(true);
			}
		});
		
		menu.add(restartGame);
		
		//create JmenuItem "Exit Game" Under menu, exit game
		JMenuItem exitGame = new JMenuItem("Exit Game");
		exitGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
				//Gui game = new Gui(true);
			}
		});
		
		menu.add(exitGame);
		
		//create JmenuItem "View Score" Under menu, show each player's score
		JMenuItem viewScore = new JMenuItem("View Score");
		viewScore.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				String scoreStr = "";
				for(int i = 0; i< Player.playerList.size(); i++) {
					scoreStr += Player.playerList.get(i).getPlayerName() + ":" + Player.playerList.get(i).getScore();
				}
				JOptionPane.showMessageDialog(null, scoreStr, "Score ", JOptionPane.INFORMATION_MESSAGE);
				//Gui game = new Gui(true);
				
			}
		});
		
		menu.add(viewScore);
		
		//create JmenuItem "Forfeit" Under menu, forfeit game and addScore to one player
		JMenuItem forfeit = new JMenuItem("Forfeit");
		forfeit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				if(turn == PieceColor.White) {
					playerWhite.addScore();
				}
				else {
					playerBlack.addScore();
				}
				JOptionPane.showMessageDialog(null, "Forfeit");
				Gui game = new Gui(true);
			}
		});
		
		menu.add(forfeit);
		return menu;
	}
	
	
	/*
	 *Gui constructor
	 *@param restart
	 *if the game is restart, then players do not need to enter their names again
	 *else, both players enter their names.
	*/
	public Gui(Boolean restart) {
		//System.out.println("103");
		//menu
		this.bPanel = new BoardPanel();
		JMenuBar menuBar = createMenu();
		this.setJMenuBar(menuBar);
		
		this.setSize(new Dimension(100,100));
		//System.out.println("110");
		
		
		//board & piece
		this.chessboard.initialBoard();
		
		initialPieces(grids);
		this.getContentPane().add(this.bPanel);
		this.pack();
		this.setLocationRelativeTo(null);
		//System.out.println("118");
		
		
		if(!restart) {
			//if start, enter players' names. Names cannot be repeated
			String whiteName = null;
			String blackName = null;
			while(whiteName == null) {
				whiteName = JOptionPane.showInputDialog("Please Enter White Player's Name");
			}
			while(blackName == null) {
				blackName = JOptionPane.showInputDialog("Please Enter Black Player's Name");
				if(blackName.contentEquals(whiteName)) {
					JOptionPane.showMessageDialog(null, "Name already taken. Change another name.");
					blackName = null;
				}
			}
			playerWhite = new Player(whiteName, PieceColor.White);
			playerBlack = new Player(blackName, PieceColor.Black);
			Player.playerList.add(playerWhite);
			Player.playerList.add(playerBlack);
		}
		else {
			playerWhite = Player.playerList.get(Player.playerList.size() - 2);
			playerBlack = Player.playerList.get(Player.playerList.size()-1);
		}
		this.setVisible(true);
	}
	
	/*
	 * Assign Jbutton pieces to correct icon
	* @param pieceButtons
	* @return void
	*/
	public void initialPieces(JButton[][] pieceButtons) {
		for( int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
                if((i+j)%2==0){
                	if (i<=7 && j<=7) {
	                    if(i == 0 && j == 0) {
	                    	pieceButtons[j][i].setIcon(blackRook);
	                    }
	                    if(i == 2 && j == 0) {
	                    	pieceButtons[j][i].setIcon(blackBishop);
	                    }
	                    if(i == 4 && j == 0) {
	                    	pieceButtons[j][i].setIcon(blackKing);
	                    }
	                    if(i == 6 && j == 0) {
	                    	pieceButtons[j][i].setIcon(blackKnight);
	                    }
	                    
	                    if(i == 1 && j == 7) {
	                    	pieceButtons[j][i].setIcon(whiteKnight);
	                    }
	                    if(i == 3 && j == 7) {
	                    	pieceButtons[j][i].setIcon(whiteQueen);
	                    }
	                    if(i == 5 && j == 7) {
	                    	pieceButtons[j][i].setIcon(whiteBishop);
	                    }
	                    if(i == 7 && j == 7) {
	                    	pieceButtons[j][i].setIcon(whiteRook);
	                    }
	                    
	                    
	                    if(j == 1) {
	                    	pieceButtons[j][i].setIcon(blackPawn);
	                    }
	                    if(j == 6) {
	                    	pieceButtons[j][i].setIcon(whitePawn);
	                    }
                	}
                }
                else{
                	if (i<=7 && j<=7) {
	                	
	                    //jLabel.setIcon();
	                    if(i == 1 && j == 0) {
	                    	pieceButtons[j][i].setIcon(blackKnight);
	                    }
	                    if(i == 3 && j == 0) {
	                    	pieceButtons[j][i].setIcon(blackQueen);
	                    }
	                    if(i == 5 && j == 0) {
	                    	pieceButtons[j][i].setIcon(blackBishop);
	                    }
	                    if(i == 7 && j == 0) {
	                    	pieceButtons[j][i].setIcon(blackRook);
	                    }
	                    
	                    if(i == 0 && j == 7) {
	                    	pieceButtons[j][i].setIcon(whiteRook);
	                    }
	                    if(i == 2 && j == 7) {
	                    	pieceButtons[j][i].setIcon(whiteBishop);
	                    }
	                    if(i == 4 && j == 7) {
	                    	pieceButtons[j][i].setIcon(whiteKing);
	                    }
	                    if(i == 6 && j == 7) {
	                    	pieceButtons[j][i].setIcon(whiteKnight);
	                    }
	                    
	                    if(j == 1) {
	                    	pieceButtons[j][i].setIcon(blackPawn);
	                    }
	                    if(j == 6) {
	                    	pieceButtons[j][i].setIcon(whitePawn);
	                    }
                	}
                }
			}
		}
	}
	
	/*
	 * getImageIcon of current position's piece
	* @param rank, file
	* @return ImageIcon
	*/
	public ImageIcon getImageIcon(int rank, int file) {
		String desc = ((ImageIcon)grids[rank][file].getIcon()).getDescription();
		if(desc.equals(blackRook.getDescription())){
		    return blackRook;
		}
		else if (desc.equals(blackBishop.getDescription())){
		    return blackBishop;
		}
		else if (desc.equals(blackKing.getDescription())){
		    return blackKing;
		}
		else if (desc.equals(blackQueen.getDescription())){
		    return blackQueen;
		}
		else if (desc.equals(blackKnight.getDescription())){
		    return blackKnight;
		}
		else if (desc.equals(blackPawn.getDescription())){
		    return blackPawn;
		}
		else if(desc.equals(whiteRook.getDescription())){
		    return whiteRook;
		}
		else if (desc.equals(whiteBishop.getDescription())){
		    return whiteBishop;
		}
		else if (desc.equals(whiteKing.getDescription())){
		    return whiteKing;
		}
		else if (desc.equals(whiteQueen.getDescription())){
		    return whiteQueen;
		}
		else if (desc.equals(whiteKnight.getDescription())){
		    return whiteKnight;
		}
		else {
		    return whitePawn;
		}
	}
	
	/*
	 * create initial chessboard with color
	* @param grids
	* @return void
	*/
	public void createEmptyBoard(JButton[][] grids) {
		for(int rank = 0; rank < 8; rank++) {
			for (int file = 0; file < 8; file++) {
				JButton button = new JButton();
				if((rank + file) % 2==0){
					button.setBackground(Color.PINK);
					button.setOpaque(true);
					button.setBorderPainted(false);
				}
				else {
					button.setBackground(Color.gray);
					button.setOpaque(true);
					button.setBorderPainted(false);
				}
				grids[rank][file] = button;
				grids[rank][file].addActionListener(this);
			}
		}
	}
	
	
	boolean isOrigin = true;
	int rank;
	int file;
	PieceColor color;
	String pieceType;
	@Override
	/*
	 * assignment movements of pieces
	* @param event
	* @return void
	*/
	public void actionPerformed(ActionEvent event) {
		//System.out.println("319");
		JButton piece = (JButton) event.getSource();
		int r = -1;
		int f = -1;
		for(int i = 0; i < 8; i++) {
			for(int j = 0;j < 8; j++) {
				if(piece == grids[i][j]) {
					r = i; 
					f = j;
					//System.out.println(isOrigin);
					//System.out.println("r: ");
					//System.out.println(r);
					//System.out.println("f:");
					//System.out.println(f);
				}
			}
		}
		//if the click if the first, then the position is the origin position
		if(isOrigin) {
			if(chessboard.getChessBoard()[r][f] != null) {
				if(chessboard.getChessBoard()[r][f].getPieceColor() != turn) {
					JOptionPane.showMessageDialog(null, "It's not your turn.");
					return;
				}
				rank = r;
				file = f;
				color = chessboard.getChessBoard()[rank][file].getPieceColor();
				pieceType =  chessboard.getChessBoard()[rank][file].getClass().getName();
				isOrigin = false;
			}
			else {
				isOrigin = false;
			}
		}
		else {
			//the position is the destination
			isOrigin = true;
			//move piece by getDescription and setIcon
			if(chessboard.movePiece(rank, file, r, f)) {
				String desc = ((ImageIcon)grids[rank][file].getIcon()).getDescription();
				if(desc.equals(blackRook.getDescription())){
				    grids[r][f].setIcon(blackRook);
				}
				else if (desc.equals(blackBishop.getDescription())){
					grids[r][f].setIcon(blackBishop);;
				}
				else if (desc.equals(blackKing.getDescription())){
					grids[r][f].setIcon(blackKing);;
				}
				else if (desc.equals(blackQueen.getDescription())){
					grids[r][f].setIcon(blackQueen);;
				}
				else if (desc.equals(blackKnight.getDescription())){
					grids[r][f].setIcon(blackKnight);;
				}
				else if (desc.equals(blackPawn.getDescription())){
					grids[r][f].setIcon(blackPawn);;
				}
				else if(desc.equals(whiteRook.getDescription())){
					grids[r][f].setIcon(whiteRook);;
				}
				else if (desc.equals(whiteBishop.getDescription())){
					grids[r][f].setIcon(whiteBishop);;
				}
				else if (desc.equals(whiteKing.getDescription())){
					grids[r][f].setIcon(whiteKing);;
				}
				else if (desc.equals(whiteQueen.getDescription())){
					grids[r][f].setIcon(whiteQueen);;
				}
				else if (desc.equals(whiteKnight.getDescription())){
					grids[r][f].setIcon(whiteKnight); ;
				}
				else {
					grids[r][f].setIcon(whitePawn);;
				}
				
				grids[rank][file].setIcon(null);
				if((rank + file)%2 == 0) {
					grids[rank][file].setBackground(Color.pink);
				}
				else {
					grids[rank][file].setBackground(Color.gray);
				}
				
				PieceColor temp;
				
				if (turn == PieceColor.Black) {
					temp = PieceColor.White;
				}
				else {
					temp = PieceColor.Black;
				}
				
				if(chessboard.getKing(PieceColor.Black) == null) {
					JOptionPane.showMessageDialog(null, "Player White wins");
					playerBlack.addScore();
				}
				if(chessboard.getKing(PieceColor.White) == null) {
					JOptionPane.showMessageDialog(null, "Player Black wins");
					playerWhite.addScore();
				}
				
				//checkmate condition
				if(chessboard.checkMate(temp)) {
					if (turn ==PieceColor.Black){
						JOptionPane.showMessageDialog(null, "Player Black is in checkmate");
						playerWhite.addScore();
					}
					else {
						JOptionPane.showMessageDialog(null, "Player White is in checkmate");
						playerBlack.addScore();
					}
				}
				//stalemate condition
				else if (chessboard.staleMate(turn)) {
					if (turn ==PieceColor.White){
						JOptionPane.showMessageDialog(null, "Player Black is in stalemate");
						playerWhite.addScore();
					}
					else {
						JOptionPane.showMessageDialog(null, "Player White is in stalekmate");
						playerBlack.addScore();
					}
				}
				else {
					//incheck condition
					if(chessboard.inCheck(turn)) {
						if (turn ==PieceColor.White){
							JOptionPane.showMessageDialog(null, "Player White is in check");
						}
						else {
							JOptionPane.showMessageDialog(null, "Player Black is in check");
						}	
					}
				}
				//switch turn
				if(turn == PieceColor.White) {
					turn = PieceColor.Black;
				}
				else {
					turn = PieceColor.White;
				}		
				
			}
			//illegal move
			else {
				JOptionPane.showMessageDialog(null, "Illigal Move");
				
			}
		}
	}
}