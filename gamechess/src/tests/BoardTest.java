package tests;


import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;
import pieces.Pawn;
import pieces.Piece;

class BoardTest{

	Board board;
	
	@Test
	public void constructorBoardTest() {
		board = new Board();
		board.initialBoard();
		assertFalse(board.movePiece(1, 0, 4, 0)); //pawn cannot move
		assertEquals(1, board.getPiece(1, 0).getPieceRank());
		assertEquals(0, board.getPiece(1, 0).getPieceFile());
		assertTrue(board.getPiece(1, 0) instanceof Pawn);
		assertTrue(board.getPiece(1, 0).firstMove());
		assertEquals(PieceColor.White, board.getPiece(1, 0).getPieceColor());
		assertNull(board.getPiece(4, 0));
		for (int i = 0; i < 8; i++) {
			assertNotNull(board.getPiece(0, i));
			assertNotNull(board.getPiece(1, i));
			assertNotNull(board.getPiece(6, i));
			assertNotNull(board.getPiece(7, i));
		}
	}	
	
	@Test
	public void outOfBoundCheck() {
		board = new Board();
		assertTrue(board.outOfBound(-1,3));
		assertTrue(board.outOfBound(-1, -4));
		assertTrue(board.outOfBound(0, 10));
		assertTrue(board.outOfBound(8, 10));
		
		assertFalse(board.setPiece("King", 8, 1, PieceColor.Black));
		
		assertFalse(board.movePiece(0, 0, 1, 1));
		
		board.setPiece("Rook", 7, 1, PieceColor.Black);
		assertFalse(board.movePiece(7, 1, 10, 4));
		
		assertTrue(board.canMove(PieceColor.White));
	}
	
	@Test
	public void getPieceAfterMovePiece() {
		board = new Board();
		board.setPiece("Rook", 2, 1, PieceColor.Black);
		board.setPiece("Rook", 3, 1, PieceColor.White);
		board.movePiece(2, 1, 3, 1);
		
		assertNull(board.getPiece(2, 1));
		assertEquals(PieceColor.Black, board.getPiece(3,1).getPieceColor());
		assertEquals(3, board.getPiece(3, 1).getPieceRank());
		assertEquals(1, board.getPiece(3, 1).getPieceFile());
		assertFalse(board.getPiece(3, 1).firstMove());
	}
	
	@Test
	public void initalBoardTest() {
		board = new Board();
		board.initialBoard();

	}
	
	@Test
	public void inCheckTest() {
		board = new Board();
		board.setPiece("King", 4, 4, PieceColor.Black);
		board.setPiece("Rook", 2, 4, PieceColor.White);
		assertTrue(board.inCheck(PieceColor.White));
	}
	
	@Test
	public void checkMateTest() {
		/* white wins */
		board = new Board();
		board.setPiece("King", 7, 4, PieceColor.Black);
		board.setPiece("King", 0, 4, PieceColor.White);
		board.setPiece("Rook", 7, 0, PieceColor.White);
		board.setPiece("Rook", 6, 1, PieceColor.White);
		
		assertTrue(board.inCheck(PieceColor.White));
		assertFalse(board.canMove(PieceColor.White));
		assertEquals(7, board.getPiece(7, 4).getPieceRank());
		assertEquals(4, board.getPiece(7, 4).getPieceFile());
		assertEquals(PieceColor.Black, board.getPiece(7, 4).getPieceColor());
		assertTrue(board.checkMate(PieceColor.White));
		assertTrue(board.getPiece(7,4).firstMove());
	}
	
	@Test
	public void notCheckMate() {
		board = new Board();
		board.setPiece("King", 7, 4, PieceColor.Black);
		board.setPiece("King", 0, 4, PieceColor.White);
		board.setPiece("Rook", 6, 1, PieceColor.White);
		assertFalse(board.checkMate(PieceColor.White));
	}
	
	@Test
	public void staleMateTest() {
		board =  new Board();
		board.setPiece("King", 6, 5, PieceColor.White);
		board.setPiece("Queen", 5, 6, PieceColor.White);
		board.setPiece("King", 7, 7, PieceColor.Black);
		assertTrue(board.staleMate(PieceColor.White));
	}
	
	@Test
	public void notStaleMateTest() {
		board =  new Board();
		board.setPiece("King", 6, 5, PieceColor.White);
		board.setPiece("Queen", 5, 6, PieceColor.White);
		board.setPiece("King", 1, 1, PieceColor.Black);
		assertFalse(board.staleMate(PieceColor.White));
	}
}
