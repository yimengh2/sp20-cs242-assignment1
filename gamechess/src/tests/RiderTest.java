package tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;

class RiderTest{

	Board board;
	
	@Test
	public void simpleWrongDest() {
		board = new Board();
		board.setPiece("Rider", 4, 4, PieceColor.Black);
		
		assertFalse(board.movePiece(4, 4, 5, 5));
		assertFalse(board.movePiece(4, 4, 6, 6));
	}
	
	@Test
	public void uprightTest() {
		board = new Board();
		board.setPiece("Rider", 4, 4, PieceColor.Black);
		board.setPiece("Rider", 5, 5, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 5, 5));
		assertTrue(board.movePiece(4, 4, 6, 6));
	}
	
	@Test
	public void uprightOutOfBoundTest() {
		board = new Board();
		board.setPiece("Rider", 6, 6, PieceColor.Black);
		board.setPiece("Rider", 7, 7, PieceColor.Black);
		assertFalse(board.movePiece(6, 6, 4, 4));
		assertFalse(board.movePiece(6, 6, 8, 8));
	}
	
	
}