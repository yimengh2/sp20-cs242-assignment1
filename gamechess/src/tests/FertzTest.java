package tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;

class FertzTest{
	Board board;
	
	
	
	@Test
	public void move_up_left() {
		board = new Board();
		board.setPiece("Fertz", 4, 4, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 5, 5));
	}
	
	@Test
	public void move_up_left_notnull() {
		board = new Board();
		board.setPiece("Fertz", 4, 4, PieceColor.Black);
		board.setPiece("King", 5, 5, PieceColor.White);
		assertTrue(board.movePiece(4, 4, 5, 5));
	}
	
	@Test
	public void move_down_left_notnull() {
		board = new Board();
		board.setPiece("Fertz", 4, 4, PieceColor.Black);
		board.setPiece("King", 3, 5, PieceColor.White);
		assertTrue(board.movePiece(4, 4, 3, 5));
	}
	
	
	@Test
	public void move() {
		board = new Board();
		board.setPiece("Fertz", 4, 4, PieceColor.Black);
		board.setPiece("King", 4, 5, PieceColor.Black);
		assertTrue(board.movePiece(4, 4, 5, 5));
	}

}
	