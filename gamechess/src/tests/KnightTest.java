package tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;

class KnightTest{
	Board board;
	
	@BeforeEach
	public void init() {
		board = new Board();
		board.setPiece("Knight", 4, 4, PieceColor.Black);
	}
	
	@Test
	public void simpleMoveTest() {
		assertTrue(board.movePiece(4, 4, 2, 5));
		assertTrue(board.movePiece(2, 5, 3, 7));
		assertTrue(board.movePiece(3, 7, 1, 6));
		assertFalse(board.movePiece(1, 6, 0, 8));
		
		assertFalse(board.movePiece(4, 4, 2, 5));
	}
	
	@Test
	public void wrongDirectionTest() {
		assertFalse(board.movePiece(4, 4, 4, 3));
	}
	
	@Test
	public void outOfBoundTest() {
		board.setPiece("Knight", 1, 1, PieceColor.Black);
		assertFalse(board.movePiece(1, 1, 0, -1));
	}
	
}