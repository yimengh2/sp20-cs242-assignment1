package tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;

class KingTest{
	Board board;
	
	@BeforeEach
	public void init() {
		board = new Board();
		board.setPiece("King", 4, 4, PieceColor.Black);
		
	}
	
	@Test
	public void outOfBoundTest() {
		board.setPiece("King", 1, 0, PieceColor.White);
		assertFalse(board.movePiece(1, 0, 1, -1));
	}
	
	@Test
	public void move_left() {
		assertTrue(board.movePiece(4, 4, 3, 4));
		assertTrue(board.movePiece(3, 4, 2, 4));
	}
	
	@Test
	public void move_right() {
		assertTrue(board.movePiece(4, 4, 4, 5));
	}
	
	@Test
	public void move_upright() {
		assertTrue(board.movePiece(4, 4, 5, 5));	
	}
	
	@Test
	public void move_too_many_steps() {
		assertFalse(board.movePiece(4, 4, 4, 6));
	}
	
	@Test
	public void meetSameColorTest() {
		board.setPiece("Rook", 4, 3, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 4, 3));
	}
	
	@Test
	public void meetDifferentColorTest() {
		board.setPiece("Rook", 4, 3, PieceColor.White);
		assertTrue(board.movePiece(4, 4, 4, 3));
	}
	
}