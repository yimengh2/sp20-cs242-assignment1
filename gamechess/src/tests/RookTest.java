package tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;

class RookTest{

	Board board;
	
	@BeforeEach
	public void init() {
		board = new Board();
		board.setPiece("Rook", 4, 4, PieceColor.Black);
	}
	
	@Test
	public void move_outbound() {
		assertFalse(board.movePiece(4, 4, -1, 4));
		assertFalse(board.movePiece(4, 4, 4, 9));
	}
	
	@Test
	public void move_left() {
		assertTrue(board.movePiece(4, 4, 3, 4));
	}
	
	@Test
	public void move_() {
		assertTrue(board.movePiece(4, 4, 6, 4));
	}
	
	@Test
	public void move_to_enemy_position() {
		board.setPiece("Rook", 4, 3, PieceColor.White);
		assertTrue(board.movePiece(4, 4, 4, 3));
	}
	
	@Test
	public void pass_enemy_position() {
		board.setPiece("Rook", 4, 3, PieceColor.White);
		assertFalse(board.movePiece(4, 4, 4, 2));
		board.setPiece("Rook", 4, 5, PieceColor.White);
		assertFalse(board.movePiece(4, 4, 4, 7));
	}
	
	@Test
	public void move_to_my_team_position() {
		board.setPiece("Rook", 4, 3, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 4, 3));
	}
	
	@Test
	public void move_to_my_position() {
		board.setPiece("Rook", 4, 3, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 4, 2));
	}
}