package tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;

class QueenTest{

	Board board;
	
	@BeforeEach
	public void init() {
		board = new Board();
		board.setPiece("Queen", 4, 4, PieceColor.Black);
	}
	
	@Test
	public void move_left() {
		assertTrue(board.movePiece(4, 4, 3, 4));
	}
	
	@Test
	public void move_up_right() {
		assertTrue(board.movePiece(4, 4, 5, 5));
	}
	
	@Test
	public void move_up_right_enemy() {
		board.setPiece("Queen", 5, 5, PieceColor.White);
		assertTrue(board.movePiece(4, 4, 5, 5));
	}
	
	@Test
	public void move_up_right_enemy2() {
		board.setPiece("Queen", 5, 5, PieceColor.White);
		assertFalse(board.movePiece(4, 4, 6, 6));
	}
	
	@Test
	public void false_move() {
		assertFalse(board.movePiece(4, 4, 5, 6));
	}
	
	@Test
	public void meetSameColor() {
		board.setPiece("Queen", 5, 5, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 5, 5));
	}
	
	@Test
	public void meetSameColor2() {
		board.setPiece("Queen", 3, 3, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 3, 3));
	}
	
	@Test
	public void cannotPassSameColor() {
		board.setPiece("Queen", 5, 5, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 6, 6));
	}
	
	@Test
	public void cannotPassSameColor2() {
		board.setPiece("Queen", 3, 3, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 1, 1));
	}	
}