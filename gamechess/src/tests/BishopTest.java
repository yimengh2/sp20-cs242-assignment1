package tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;

class BishopTest{

	Board board;
	
	@BeforeEach
	public void init() {
		board = new Board();
		board.setPiece("Bishop", 4, 4, PieceColor.Black);
	}
	
	@Test
	public void moveTest() {
		assertTrue(board.movePiece(4, 4, 3, 3));
		assertTrue(board.movePiece(3, 3, 2, 4));
		assertFalse(board.movePiece(2, 4, 2, 3));
		assertTrue(board.movePiece(2, 4, 4, 2));
	}
	
	@Test
	public void nullTest() {
		assertNull(board.getPiece(2, 2));
		assertFalse(board.movePiece(2, 2, 1, 1));
	}
	
	@Test
	public void meetSameColorPiece() {
		board.setPiece("Bishop", 3, 3, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 3, 3));
	}
	
	@Test
	public void cannotPassSameColorPiece() {
		board.setPiece("Bishop", 3, 3, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 1, 1));
	}
	
	@Test
	public void cannotPassDifferentColorPiece() {
		board.setPiece("Bishop", 3, 3, PieceColor.White);
		assertFalse(board.movePiece(4, 4, 1, 1));
	}
	
	@Test
	public void meetDifferentColorPiece() {
		board.setPiece("Bishop", 3, 3, PieceColor.White);
		assertTrue(board.movePiece(4, 4, 3, 3));
	}
}