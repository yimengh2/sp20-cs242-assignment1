package tests;

import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import board.Board;
import pieces.PieceColor;

class PawnTest{
	Board board;
	
	@BeforeEach
	public void init() {
		board = new Board();
		board.setPiece("Pawn", 4, 4, PieceColor.Black);
	}
	
	@Test
	public void outOfBound() {
		board.setPiece("Pawn", 7, 4, PieceColor.Black);
		assertFalse(board.movePiece(7, 4, 8, 4));		
	}
	
	@Test
	public void outOfBoundTwoStep() {
		board.setPiece("Pawn", 6, 4, PieceColor.Black);
		assertFalse(board.movePiece(6, 4, 8, 4));		
	}
	
	@Test
	public void move_up1() {
		assertTrue(board.movePiece(4, 4, 5, 4));		
	}
	
	@Test
	public void move_up2() {
		assertTrue(board.movePiece(4, 4, 6, 4));
		assertTrue(board.movePiece(6, 4, 7, 4));
		assertFalse(board.movePiece(7, 4, 8, 4));
	}
	
	@Test
	public void second_move() {
		assertTrue(board.movePiece(4, 4, 5, 4));
		//assertFalse(board.movePiece(4, 4, 7, 4));
		
	}
	
	@Test
	public void capture_diagonal_right() {
		board.setPiece("Pawn", 5, 5, PieceColor.White);
		assertTrue(board.movePiece(4, 4, 5, 5));
	}
	
	@Test
	public void capture_diagonal_left() {
		board.setPiece("Pawn", 5, 3, PieceColor.White);
		assertTrue(board.movePiece(4, 4, 5, 3));
	}
	
	@Test
	public void false_capture() {
		board.setPiece("Pawn", 5, 5, PieceColor.Black);
		assertFalse(board.movePiece(4, 4, 5, 5));
	}
	
	
}