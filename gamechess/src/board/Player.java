package board;

import java.util.List;
import java.util.ArrayList;

import pieces.Piece;
import pieces.PieceColor;

public class Player {
	
	private PieceColor color;
	private int score;
	private String playerName;
	//public static Player playerWhite;
	//public static Player playerBlack;
	public static List <Player> playerList = new ArrayList <Player>();
	public Player(String playerName, PieceColor myColor) {
		this.score = 0;
		this.playerName = playerName;
		this.color = myColor;
	}
	
	public void setColor(PieceColor color) {
		this.color = color;
	}
	
	public PieceColor getColor() {
		return this.color;
	}
	
	public void addScore() {
		this.score+=1;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public String getPlayerName() {
		return this.playerName;
	}
	
	
	
	
	
	
	
	
}
