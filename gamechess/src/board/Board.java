package board;
import pieces.Piece;

import pieces.Position;
import pieces.PieceColor;
import pieces.King;
import pieces.Bishop;
import pieces.Knight;
import pieces.Pawn;
import pieces.Queen;
import pieces.Rook;
import pieces.Fertz;
import pieces.Rider;


import java.util.ArrayList;



/* 
 * The Board class provides board constructor, initialization and major implementations.
 */
public class Board{
	private Piece[][] chessboard;
	
	
	/*
	* Default board constructor
	* Construct a 8*8 <em>Board</em> with all position null.
	*/
	public Board() {
		chessboard = new Piece [8][8];
		for (int rank =0; rank < 8; rank ++) {
			for (int file = 0; file < 8; file ++) {
				chessboard[rank][file]=null;
			}
		}
	}
	
	/*
	* return board
	*/
    public Piece[][] getChessBoard() {
        return this.chessboard;
    }

	
	/*
	* Copy Constructor Baord
	* Construct a 8*8 <em>Board</em> with all position pointing to origin_board.
	*/
	public Board(Board origin_board) {
		//for shallow copy a existed board
		chessboard = new Piece [8][8];
		for (int rank = 0; rank < 8; rank ++) {
			for (int file =0; file < 8; file++) {
				chessboard[rank][file] = origin_board.chessboard[rank][file];
			}
		}
	}
	
	/*
	* @param myColor
	* 	get color of my side
	* @return Position
	* 	return opponent's position of King
	*/
	public Position getKing(PieceColor mycolor) {
		for (int rank = 0; rank < 8; rank ++) {
			for (int file = 0; file < 8; file ++) {
				if (chessboard[rank][file] != null && chessboard[rank][file].getPieceColor() == mycolor && chessboard[rank][file] instanceof King) {
					return new Position(rank, file);
				}
			}
		}
		return null;
	}

	/*
	* If I can check opponent, return true.
	* @param myColor
	* 	get color of my side
	* @return boolean
	* 	return whether any of my valid movements can check my opponent
	*/
	public boolean inCheck(PieceColor mycolor) {
		//see if mycolor piece can capture opponent's color
		PieceColor opponent_color;
		if (mycolor == PieceColor.White) {
			opponent_color = PieceColor.Black;
		}
		else {
			opponent_color = PieceColor.White;
		}
		
		if (getKing(opponent_color) ==null) {
			//for test, should not happen in real condition.
			return false;
		}
		
		int target_x = getKing(opponent_color).getRank();
		int target_y = getKing(opponent_color).getFile();
		
		for (int rank = 0; rank < 8; rank ++) {
			for(int file =0; file < 8; file ++) {
				if (chessboard[rank][file] != null && chessboard[rank][file].getPieceColor() == mycolor && chessboard[rank][file].isLegalMove(target_x, target_y)) {
					return true;
				}
			}
		}
		return false;
	}

	/*
	*  After move pieces, update validmoveslist for every piece
	* @param 
	* @return void
	*/
	public void setAllValidMovesList() {
		for (int rank = 0; rank < 8; rank ++) {
			for (int file =0; file <8; file ++) {
				if (chessboard[rank][file] != null) {
					chessboard[rank][file].setValidMovesList(rank, file, chessboard);
				}
			}
		}
	}
	
	/*
	* @param rank
	* 	rank position
	* @param file
	* 	file position
	* @return User
	* 	return whether the position is outofbound
	*/
	public boolean outOfBound (int rank, int file) {
		if (rank < 0 || file < 0 || rank >=8 || file >= 8) {
			return true;
		}
		else return false;
	}
	
	/*
	* If isLegalMove, check if after move incheck.
	* If incheck, revert the board and return false, meaning cannot move.
	* @param origin_x
	* 	origin rank position
	* @param origin_y
	* 	origin file position
	* @param dest_x
	* 	dest rank position
	* @param dest_y
	* 	dest file position
	* @return boolean
	* 	whether we can move the piece without later being incheck
	*/
	public boolean movePiece(int origin_x, int origin_y, int dest_x, int dest_y) {
		if(chessboard[origin_x][origin_y] == null || outOfBound(dest_x, dest_y)) {
			//if origin position is empty or outofbound, return false
			return false;
		}
		PieceColor opponentColor = chessboard[origin_x][origin_y].getPieceColor();
		PieceColor myColor;
		if (opponentColor == PieceColor.White) {
			myColor = PieceColor.Black;
		}
		else {
			myColor =  PieceColor.White;
		}
		if (chessboard[origin_x][origin_y].isLegalMove(dest_x, dest_y)) {
			Board originBoard = new Board(this);
			boolean origin_first_move = chessboard[origin_x][origin_y].firstMove();
			PieceColor originColor = chessboard[origin_x][origin_y].getPieceColor();
			
			boolean dest_first_move = false; //random initialize
			PieceColor destColor = PieceColor.White;
			boolean destIsNull = true;
			if (chessboard[dest_x][dest_y] != null) {
				destIsNull = false;
				dest_first_move = chessboard[dest_x][dest_y].firstMove();
				destColor = chessboard[dest_x][dest_y].getPieceColor();
				
			}
			chessboard[dest_x][dest_y] = chessboard[origin_x][origin_y];
			chessboard[origin_x][origin_y] = null;
			chessboard[dest_x][dest_y].setFirstMove(false);
			chessboard[dest_x][dest_y].setPosition(new Position(dest_x, dest_y));
			chessboard[dest_x][dest_y].setPieceColor(opponentColor);
			
			setAllValidMovesList();
			
			if (this.inCheck(myColor)) {
				//if incheck after movePiece, revert original piece
				chessboard = originBoard.chessboard;
				chessboard[origin_x][origin_y].setPosition(new Position(origin_x, origin_y));
				chessboard[origin_x][origin_y].setFirstMove(origin_first_move);
				chessboard[origin_x][origin_y].setPieceColor(originColor);
		
				if (destIsNull == false) {
					//revert dest piece
					chessboard[dest_x][dest_y].setPosition(new Position(dest_x, dest_y));
					chessboard[dest_x][dest_y].setFirstMove(dest_first_move);
					chessboard[dest_x][dest_y].setPieceColor(destColor);
				}
				setAllValidMovesList();
				return false;
			}
			else {
				return true;
			}
		}
		return false;
	}
	
	/*
	* @param rank
	* @param file
	* @return Piece
	* 	return Piece in position (rank, file)
	*/
	public Piece getPiece(int rank, int file) {
		if (chessboard[rank][file] == null) {
			return null;
		}
		return chessboard[rank][file];
	}
	
	/*
	* set Piece by pieceNmae
	* @param pieceName
	* @param rank
	* @param file
	* @param pColor
	* @return boolean
	* 	if outofbound, return false
	*/
	public boolean setPiece(String pieceName, int rank, int file, PieceColor pColor) {
		if (!outOfBound(rank, file)) {
			switch (pieceName) {
			case "Rook": 
				chessboard[rank][file] = new Rook(new Position(rank, file), pColor, true);
				break;	
			case "Bishop":
				chessboard[rank][file] = new Bishop(new Position(rank, file), pColor, true);
				break;
			case "Knight":
				chessboard[rank][file] = new Knight(new Position(rank, file), pColor, true);
				break;
			case "King":
				chessboard[rank][file] = new King(new Position(rank, file), pColor, true);
				break;
			case "Queen":
				chessboard[rank][file] = new Queen(new Position(rank, file), pColor, true);
				break;
			case "Pawn":
				chessboard[rank][file] = new Pawn(new Position(rank, file), pColor, true);
				break;
			case "Fertz":
				chessboard[rank][file] = new Fertz(new Position(rank, file), pColor, true);
				break;
			case "Rider":
				chessboard[rank][file] = new Rider(new Position(rank, file), pColor, true);
				break;
			}
			setAllValidMovesList();
			return true;
		}
		return false;
	}
	
	/*
	* It's opponent's turn.
	* If opponent cannot move, return true;
	* @param myColor
	* @return boolean
	*/
	public boolean canMove(PieceColor myColor) {
		for (int rank = 0; rank < 8; rank++) {
			for (int file = 0; file < 8; file ++) {
				if (chessboard[rank][file] != null && chessboard[rank][file].getPieceColor() != myColor) {
					ArrayList<Position> validMovesList = chessboard[rank][file].validMovesList;
					for (Position pos: validMovesList) {
						if (movePiece(rank, file, pos.getRank(), pos.getFile())) {
							return true;
						}
					}
				}
			}
		}
		return false;	
	}


	/*
	* It's opponent's turn, return boolean checkmate.
	* If returns true, myColor wins.
	* @param myColor
	* @return boolean
	*/
	public boolean checkMate(PieceColor myColor) {
		//inCheck and !canMove
		if(inCheck(myColor)) {
			if(!canMove(myColor)){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * if is stalemate return true
	* @param myColor
	* @return boolean
	*/
	public boolean staleMate(PieceColor myColor) {
		//!inCheck and !canMove
		if(!inCheck(myColor)) {
			if (!canMove(myColor)){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * initial Piece on board
	* @param 
	* @return void
	*/
	public void initialBoard() {
		this.setPiece("Rook", 0, 0, PieceColor.White);
		this.setPiece("Rook", 0, 7, PieceColor.White);
		this.setPiece("Rook", 7, 0, PieceColor.Black);
		this.setPiece("Rook", 7, 7, PieceColor.Black);
		
		this.setPiece("Knight", 0, 1, PieceColor.White);
		this.setPiece("Knight", 0, 6, PieceColor.White);
		this.setPiece("Knight", 7, 1, PieceColor.Black);
		this.setPiece("Knight", 7, 6, PieceColor.Black);
		
		this.setPiece("Bishop", 0, 2, PieceColor.White);
		this.setPiece("Bishop", 0, 5, PieceColor.White);
		this.setPiece("Bishop", 7, 2, PieceColor.Black);
		this.setPiece("Bishop", 7, 5, PieceColor.Black);
		
		this.setPiece("Queen", 0, 3, PieceColor.White);
		this.setPiece("Queen", 7, 3, PieceColor.Black);
		
		this.setPiece("King", 0, 4, PieceColor.White);
		this.setPiece("King", 7, 4, PieceColor.Black);
		
		for (int i = 0; i < 8; i++) {
			this.setPiece("Pawn", 1, i, PieceColor.White);
			this.setPiece("Pawn", 6, i, PieceColor.Black);
		}
	}	
}
